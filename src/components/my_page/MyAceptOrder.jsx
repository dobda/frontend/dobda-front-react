import './MyAceptOrder.css';
import React, { useEffect, useState, useCallback } from 'react';
import OrderItem from '../order/OrderItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';
import {
  getMyHelperOrderList,
  retryCalcAll,
  getRegisterHelperState,
  cancelRegisterHelper,
} from '../../axios/axios_actions';
import moment from 'moment-timezone';

function MyAceptOrder() {
  const userInfo = useSelector((state) => state.user.userInfo);

  const [filterType, setFilterType] = useState(0);
  const [orderList, setOrderList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [helperState, setHelperState] = useState();
  const [isLoaded, setIsLoaded] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    if (userInfo?.isHelper) {
      return;
    }
    getRegisterHelperState().then((response) => {
      setHelperState(response.data);
      setIsLoaded(true);
    });
  }, [userInfo]);

  useEffect(() => {
    setOrderList([]);
    setIsEnd(false);
    setPage(0);
  }, [filterType]);

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const filters = [filterType];
      const response = await getMyHelperOrderList(page, filters);
      setOrderList((orderList) => [...orderList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page, filterType]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const onSelectedFilterType = useCallback(
    (event) => {
      event.preventDefault();
      if (isLoading) {
        return;
      }
      setFilterType(event.currentTarget.dataset.type);
    },
    [isLoading]
  );

  const onChangeOrder = useCallback((orderInfo) => {
    if (!orderInfo) {
      return;
    }
    setOrderList((orderList) => orderList.filter((v) => v.id != orderInfo.id));
  }, []);

  const onConfirmReview = useCallback((reviewInfo) => {
    if (!reviewInfo) {
      return;
    }
    setOrderList((orderList) =>
      orderList.map((v) => {
        if (v.id == reviewInfo.orderId) {
          const newOrder = { ...v };
          if (newOrder.reviews == null) {
            newOrder.reviews = [];
          }
          newOrder.reviews.push(reviewInfo);
          return newOrder;
        }
        return v;
      })
    );
  }, []);

  const onSelectAccount = useCallback((accountInfo) => {
    if (!window.confirm('선택한 계좌로 정산 절차를 신청하시겠습니까?')) {
      return;
    }

    retryCalcAll(accountInfo?.accountHolderName, accountInfo?.fintechNum)
      .then(() => {
        alert('선택된 계좌로 정산 요청을 성공했습니다.\n새로고침을 해주세요.');
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, []);

  const onClickCancelRegister = useCallback(() => {
    if (!window.confirm('정말로 헬퍼 신청을 취소하실거에요? ㅠㅠ')) {
      return;
    }

    cancelRegisterHelper()
      .then(() => {
        alert('헬퍼 신청이 취소되었습니다.');
        setHelperState();
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, []);

  return (
    <div className='MyAceptOrder_root'>
      {userInfo?.isHelper ? (
        <>
          <div className='MyAceptOrder_menu-root'>
            <div className='MyAceptOrder_menu'>
              <div className='MyAceptOrder_menu-underline' />
              <a
                onClick={onSelectedFilterType}
                data-type='0'
                className={
                  'MyAceptOrder_menu-button' +
                  (filterType == 0 ? ' MyAceptOrder_menu-button-selected' : '')
                }
              >
                <div className='MyAceptOrder_menu-button-underline' />
                <span>수락 대기 중</span>
              </a>
              <a
                onClick={onSelectedFilterType}
                data-type='10'
                className={
                  'MyAceptOrder_menu-button' +
                  (filterType == 10 ? ' MyAceptOrder_menu-button-selected' : '')
                }
              >
                <div className='MyAceptOrder_menu-button-underline' />
                <span>수락됨</span>
              </a>
              <a
                onClick={onSelectedFilterType}
                data-type='40'
                className={
                  'MyAceptOrder_menu-button' +
                  (filterType == 40 ? ' MyAceptOrder_menu-button-selected' : '')
                }
              >
                <div className='MyAceptOrder_menu-button-underline' />
                <span>거래완료</span>
              </a>
              <a
                onClick={onSelectedFilterType}
                data-type='70'
                className={
                  'MyAceptOrder_menu-button' +
                  (filterType == 70 ? ' MyAceptOrder_menu-button-selected' : '')
                }
              >
                <div className='MyAceptOrder_menu-button-underline' />
                <span>정산 실패</span>
              </a>
            </div>
          </div>
          <div className='MyAceptOrder_list'>
            {orderList.map((v, idx) => (
              <OrderItem
                orderInfo={v}
                key={idx}
                onApprove={onChangeOrder}
                onCancel={onChangeOrder}
                onComplete={onChangeOrder}
                onRetry={onChangeOrder}
                onSelectAccount={onSelectAccount}
                onConfirmReview={onConfirmReview}
              />
            ))}
            <div ref={bottomRef} className='MyAceptOrder_bottom'>
              {isLoading && <Loader />}
            </div>
          </div>
        </>
      ) : (
        <div className='MyAceptOrder_registerHelper_root'>
          {helperState?.state == 10 ? (
            <>
              <p>
                {moment(helperState?.regTime).format('YYYY년 MM월 DD일')}에
                신청한 헬퍼 신청이 아직 심사중입니다.
              </p>
              <Link to='/helper'>
                <Button variant='secondary'>다시 신청하기</Button>
              </Link>
              <Button
                style={{ marginLeft: '10px' }}
                variant='danger'
                onClick={onClickCancelRegister}
              >
                신청 취소하기
              </Button>
            </>
          ) : helperState?.state == 20 ? (
            <>
              <p>
                {moment(helperState.regTime).format('YYYY년 MM월 DD일')}에
                신청한 헬퍼 신청이 거부되었습니다.
              </p>
              <Link to='/helper'>
                <Button variant='secondary'>다시 신청하기</Button>
              </Link>
            </>
          ) : (
            isLoaded && (
              <>
                <p>아직 헬퍼가 아니에요.</p>
                <p>지금 바로 헬퍼로 등록해보세요.</p>
                <Link to='/helper'>
                  <Button>헬퍼 등록 신청하러 가기</Button>
                </Link>
              </>
            )
          )}
        </div>
      )}
    </div>
  );
}

export default MyAceptOrder;
