import './DaumPostModal.css';

import React, { useCallback, useEffect } from 'react';
import DaumPostCode from 'react-daum-postcode';
import { createPortal } from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

function DaumPostModal({ closeWhenClickBack, onClose, onComplete }) {
  useEffect(() => {
    document.body.style.overflow = 'hidden';
    return () => {
      document.body.style.overflow = 'unset';
    };
  }, []);

  const onClickBack = useCallback(
    (event) => {
      event.stopPropagation();
      closeWhenClickBack && onClose?.();
    },
    [closeWhenClickBack, onClose]
  );

  return createPortal(
    <div className='DaumPostModal_back' onClick={onClickBack}>
      <div className='DaumPostModal_root' onClick={(e) => e.stopPropagation()}>
        <div className='DaumPostModal_bar'>
          <div className='DaumPostModal_bar-side' />
          <span>주소 검색</span>
          <div className='DaumPostModal_bar-side' onClick={onClose}>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        <DaumPostCode
          className='DaumPostModal_postcode'
          onComplete={(data) => {
            onClose();
            onComplete(data);
          }}
        />
      </div>
    </div>,
    document.getElementById('backClickModal')
  );
}

export default DaumPostModal;
