import './ChatRoomItem.css';

import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { setChatRoomInfo } from '../../_redux/actions/modal_action';
import moment from 'moment';

function ChatRoomItem({ chatRoomInfo }) {
  const dispatch = useDispatch();

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const onSelectChatRoom = useCallback(() => {
    dispatch(setChatRoomInfo(chatRoomInfo));
  });

  return (
    <div className='ChatRoomItem_root'>
      <div>
        <div className='ChatRoomItem_imgbox'>
          <img src='' onError={onFallbackImg} />
        </div>
      </div>
      <div className='ChatRoomItem_desc' onClick={onSelectChatRoom}>
        <span>{chatRoomInfo?.partnerNickname}</span>
        <span>{chatRoomInfo?.lastMessage}</span>
      </div>
      <div className='ChatRoomItem_right'>
        <span>
          {chatRoomInfo?.lastTime &&
            moment(chatRoomInfo.lastTime).format('YYYY/MM/DD')}
        </span>
        <div>
          {chatRoomInfo?.hasUnreadMsg && (
            <div className='ChatRoomItem_unread'></div>
          )}
          <span>
            {chatRoomInfo?.lastTime &&
              moment(chatRoomInfo.lastTime).format('HH:mm:ss')}
          </span>
        </div>
      </div>
    </div>
  );
}

export default ChatRoomItem;
