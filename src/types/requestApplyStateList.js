export default [
  {
    code: 10,
    label: '대기중',
  },
  {
    code: 20,
    label: '거절됨',
  },
  {
    code: 30,
    label: '수락됨',
  },
];
