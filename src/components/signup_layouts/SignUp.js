import './SignUp.css';
import React, { useCallback, useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import Checkbox from '@mui/material/Checkbox';
import Button from '@mui/material/Button';
import FormControlLabel from '@mui/material/FormControlLabel';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import {
  userEmailCheck,
  userNicknameCheck,
  requestAuthSms,
  checkAuthSms,
  userJoin,
} from '../../axios/axios_actions';

// 이메일 패턴
const emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
// pwd는 8~30자로 영문, 숫자, 특수문자를 한번씩은 반드시 사용해야 한다.
const pwdPattern =
  /^(?=.*[a-zA-Z])(?=.*[\d])(?=.*[!@#$%^&_*])[a-zA-Z\d!@#$%^&_*]{8,30}$/;
// 이름은 3~10자로 일반문자와 숫자로만 구성되고 반드시 첫 문자가 문자이어야 한다.
const nicknamePattern = /^\p{L}[\p{L}\d]{2,19}$/u;

function Copyright(props) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      <Link color='inherit' to='/'>
        DOBDA
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function SignUp() {
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [isValidEmail, setIsValidEmail] = useState(true);
  const [isUniqueEmail, setIsUniqueEmail] = useState(false);

  const [pwd, setPwd] = useState('');
  const [isValidPwd, setIsValidPwd] = useState(true);

  const [name, setName] = useState('');
  const [isValidName, setIsValidName] = useState(true);

  const [nickname, setNickname] = useState('');
  const [isValidNickname, setIsValidNickname] = useState(true);
  const [isUniqueNickname, setIsUniqueNickname] = useState(false);

  const [phoneNum, setPhoneNum] = useState('');
  const [isSendKeyNum, setIsSendKeyNum] = useState(false);

  const [keyNum, setKeyNum] = useState('');
  const [isValidKeyNum, setIsValidKeyNum] = useState(false);

  const [bankAuthPopup, setBankAuthPopup] = useState();
  const [bankAuthCode, setBankAuthCode] = useState();

  const onChangeEmail = useCallback((event) => {
    const email = event.target.value;
    setEmail(email);
    setIsValidEmail(emailPattern.test(email));
    setIsUniqueEmail(false);
  });

  const onChangePwd = useCallback((event) => {
    const pwd = event.target.value;
    setPwd(pwd);
    setIsValidPwd(pwdPattern.test(pwd));
  });

  const onChangeName = useCallback((event) => {
    const name = event.target.value;
    setName(name);
    setIsValidName(name.length > 1);
  });

  const onChangeNickname = useCallback((event) => {
    const nickname = event.target.value;
    setNickname(nickname);
    setIsValidNickname(nicknamePattern.test(nickname));
    setIsUniqueNickname(false);
  });

  const onChangePhoneNum = useCallback((event) => {
    const phoneNum = event.target.value;
    setPhoneNum(phoneNum);
  });

  const onChangeKeyNum = useCallback((event) => {
    const keyNum = event.target.value;
    setKeyNum(keyNum);
  });

  const onClickEmailCheck = useCallback(
    (e) => {
      e.preventDefault();
      if (email == '' || !isValidEmail) {
        return alert('이메일을 올바르게 입력해주세요.');
      }

      userEmailCheck(email)
        .then((response) => {
          if (response.data) {
            alert('회원가입 가능한 아이디 입니다.😊');
            setIsUniqueEmail(true);
          } else {
            alert('중복된 아이디 입니다.😥');
          }
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [email]
  );

  const onClickNicknameCheck = useCallback(
    (e) => {
      e.preventDefault();
      if (nickname == '' || !isValidNickname) {
        return alert('닉네임을 올바르게 입력해주세요.');
      }

      userNicknameCheck(nickname)
        .then((response) => {
          if (response.data) {
            alert('사용 가능한 닉네임 입니다.😊');
            setIsUniqueNickname(true);
          } else {
            alert('중복된 닉네임 입니다.😥');
          }
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [nickname]
  );

  const onClickRequestKey = useCallback(
    (e) => {
      e.preventDefault();
      if (phoneNum == '') {
        return alert('전화번호를 입력해주세요.');
      }

      requestAuthSms(phoneNum)
        .then(({ data }) => {
          alert(
            `인증번호가 발송되었습니다. 5분 내에 인증해주세요.😊\n테스트용이기 때문에 실제 sms는 발송하지 않고 있습니다.\n인증코드 ${data}를 입력해주세요`
          );
          setIsValidKeyNum(false);
          setIsSendKeyNum(true);
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [phoneNum]
  );

  const onClickAuthKeyNum = useCallback(
    (e) => {
      e.preventDefault();
      if (keyNum == '') {
        return alert('인증번호를 입력해주세요.😥');
      }

      checkAuthSms(phoneNum, keyNum)
        .then(() => {
          alert('인증이 성공했습니다.😊');
          setIsValidKeyNum(true);
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [phoneNum, keyNum]
  );

  const openBankAuthPopup = useCallback(() => {
    if (bankAuthPopup) {
      return;
    }

    const width = '500';
    const height = '600';
    const left = Math.ceil((window.screen.width - width) / 2);
    const top = Math.ceil((window.screen.height - height) / 2);
    const options = `top=${top}, left=${left}, width=${width}, height=${height}, status=no, menubar=no, toolbar=no, resizable=no, location=no, directories=no`;
    const popup = window.open(
      `${process.env.REACT_APP_OPEN_BANK_HOST}/oauth/2.0/authorize` +
        '?response_type=code' +
        `&client_id=${process.env.REACT_APP_OPEN_BANK_CLIENT_ID}` +
        `&redirect_uri=${process.env.REACT_APP_OPEN_BANK_REDIRECT}` +
        '&scope=login+transfer' +
        '&state=b80BLsfigm9OokPTjy03elbJqRHOfGSY' +
        '&auth_type=0',
      null,
      options
    );
    var timer = setInterval(function () {
      if (popup.closed) {
        clearInterval(timer);
        setBankAuthPopup();
      } else if (popup?.location?.pathname == '/openbank/auth') {
        setBankAuthCode(new URLSearchParams(popup.location.search).get('code'));
        popup.close();
      }
    }, 100);
    setBankAuthPopup(popup);
  }, [bankAuthPopup]);

  const onClickSignUp = useCallback(
    (e) => {
      e.preventDefault();
      if (!isUniqueEmail) {
        return alert('이메일 중복 확인을 해주세요.😥');
      }
      if (!isUniqueNickname) {
        return alert('닉네임 중복 확인을 해주세요.😥');
      }
      if (!isValidKeyNum) {
        return alert('전화번호 인증을 해주세요.😥');
      }
      if (!(bankAuthCode != null)) {
        return alert('계좌 인증을 해주세요.😥');
      }
      if (name == '' || !isValidName || pwd == '' || !isValidPwd) {
        // if (name == '' || !isValidName || pwd == '') {
        return alert('입력된 내용이 올바르지 않습니다.😥');
      }

      userJoin(email, pwd, name, nickname, phoneNum, keyNum, bankAuthCode)
        .then(() => {
          alert('회원가입을 성공했습니다.😊');
          navigate('/signIn');
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [
      email,
      pwd,
      name,
      nickname,
      phoneNum,
      keyNum,
      bankAuthCode,
      isUniqueEmail,
      isUniqueNickname,
      isValidKeyNum,
      isValidName,
      isValidPwd,
    ]
  );

  return (
    <Card className='SignUp_root' sx={{ maxWidth: 500 }}>
      <Container className='SignUp_container' component='main' maxWidth='xs'>
        <Box id='box'>
          <Typography
            id='BrandName'
            sx={{ mt: 6, mb: 3 }}
            component='h1'
            variant='h5'
          >
            DOBDA
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <TextField
                className={isValidEmail ? '' : 'SignUp_invalidate'}
                margin='normal'
                label='이메일'
                value={email}
                onChange={onChangeEmail}
                required
                fullWidth
                autoComplete='email'
                autoFocus
                sx={{ mb: 2 }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button
                className='SignUp_button'
                margin='normal'
                fullWidth
                required
                variant='contained'
                onClick={onClickEmailCheck}
                disabled={isUniqueEmail}
              >
                {isUniqueEmail ? '확인 완료' : '중복 확인'}
              </Button>
            </Grid>
          </Grid>
          <TextField
            className={isValidPwd ? '' : 'SignUp_invalidate'}
            label='비밀번호'
            type='password'
            required
            fullWidth
            value={pwd}
            onChange={onChangePwd}
            autoComplete='current-password'
            sx={{ mb: 1 }}
          />
          <p>
            * 8~30자 이내 영문,숫자,특수문자를 한번씩 조합해야 합니다.
            <br />* 사용 가능한 특수문자: !@#$%^&amp;_*
          </p>

          <TextField
            className={isValidName ? '' : 'SignUp_invalidate'}
            label='이름'
            margin='normal'
            required
            fullWidth
            value={name}
            onChange={onChangeName}
            autoComplete='given-name'
          />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <TextField
                className={isValidNickname ? '' : 'SignUp_invalidate'}
                label='닉네임'
                id='nickname'
                margin='normal'
                required
                fullWidth
                value={nickname}
                onChange={onChangeNickname}
                autoComplete='nickname'
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button
                className='SignUp_button'
                margin='normal'
                fullWidth
                required
                variant='contained'
                onClick={onClickNicknameCheck}
                disabled={isUniqueNickname}
              >
                {isUniqueNickname ? '확인 완료' : '중복 확인'}
              </Button>
            </Grid>
          </Grid>
          <p>
            * 닉네임은 3~20자 이내 일반문자, 숫자로만 구성되어야 합니다.
            <br />* 첫 문자는 일반문자만 가능합니다.
          </p>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <TextField
                label='전화번호 입력'
                id='phone_num'
                margin='normal'
                required
                fullWidth
                value={phoneNum}
                onChange={onChangePhoneNum}
                autoComplete='phone_num'
                InputProps={{
                  readOnly: isSendKeyNum,
                }}
              />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Button
                className='SignUp_button'
                margin='normal'
                fullWidth
                required
                variant='contained'
                onClick={onClickRequestKey}
              >
                {isSendKeyNum ? '재요청' : '인증키 요청'}
              </Button>
            </Grid>
          </Grid>

          {isSendKeyNum ? (
            <Grid container spacing={2}>
              <Grid item xs={12} sm={8}>
                <TextField
                  label='인증번호 입력'
                  id='code_input'
                  margin='normal'
                  required
                  fullWidth
                  value={keyNum}
                  onChange={onChangeKeyNum}
                  autoComplete='code_input'
                  InputProps={{
                    readOnly: isValidKeyNum,
                  }}
                />
              </Grid>
              <Grid item xs={12} sm={4}>
                <Button
                  className='SignUp_button'
                  margin='normal'
                  fullWidth
                  required
                  variant='contained'
                  onClick={onClickAuthKeyNum}
                  disabled={isValidKeyNum}
                >
                  {isValidKeyNum ? '인증 완료' : '확인'}
                </Button>
              </Grid>
            </Grid>
          ) : null}

          <Button
            className='SignUp_button'
            margin='normal'
            required
            fullWidth
            variant='contained'
            onClick={openBankAuthPopup}
            disabled={bankAuthCode != null}
          >
            {bankAuthCode != null ? '계좌 인증 완료' : '계좌 인증'}
          </Button>

          <Button
            id='login_btn'
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 4, mb: 3 }}
            onClick={onClickSignUp}
          >
            회원가입
          </Button>
          <Grid container id='signup_btn'>
            <Grid item xs>
              <Link to='/signIn'>로그인</Link>
            </Grid>
          </Grid>
        </Box>
        <Copyright sx={{ mt: 2, mb: 3 }} />
      </Container>
    </Card>
  );
}
