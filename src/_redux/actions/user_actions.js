import axios from 'axios';
import qs from 'qs';
import { LOGIN, MY_INFO, LOGOUT } from './types';

export function loginUser(data) {
  const result = axios.post('/login', qs.stringify(data));

  return {
    type: LOGIN,
    payload: result,
  };
}

export function loginAdmin(data) {
  const result = axios.post('/admin/login', qs.stringify(data));

  return {
    type: LOGIN,
    payload: result,
  };
}

export function myInfo() {
  const result = axios.get('/api/myInfo').then((response) => response.data);

  return {
    type: MY_INFO,
    payload: result,
  };
}

export function logout() {
  const result = axios.get('/logout');

  return {
    type: LOGOUT,
    payload: result,
  };
}
