import './MyRequest.css';
import React, { useEffect, useState, useCallback } from 'react';
import RequestItem from '../request/RequestItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { myRequestList } from '../../axios/axios_actions';

function MyRequest() {
  const [filterType, setFilterType] = useState(10);
  const [requestList, setRequestList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    setRequestList([]);
    setIsEnd(false);
    setPage(0);
  }, [filterType]);

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await myRequestList(page, [filterType]);
      setRequestList((requestList) => [
        ...requestList,
        ...response.data.content,
      ]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page, filterType]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const onSelectedFilterType = useCallback(
    (event) => {
      event.preventDefault();
      if (isLoading) {
        return;
      }
      setFilterType(event.currentTarget.dataset.type);
    },
    [isLoading]
  );

  const onChangeRequestInfo = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.map((v) =>
        v.requestId == requestInfo.requestId ? { ...requestInfo } : v
      )
    );
  }, []);

  const onDelete = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  const onRefresh = useCallback(
    (requestInfo) => {
      if (!requestInfo) {
        return;
      }
      setRequestList((requestList) => {
        const newList = requestList.filter(
          (v) => v.requestId != requestInfo.requestId
        );
        if (page <= 1) {
          newList.unshift(requestInfo);
        }
        return newList;
      });
    },
    [page]
  );

  const onHide = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  const onBlock = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  return (
    <div className='MyRequest_root'>
      <div className='MyRequest_menu-root'>
        <div className='MyRequest_menu'>
          <div className='MyRequest_menu-underline' />
          <a
            onClick={onSelectedFilterType}
            data-type='10'
            className={
              'MyRequest_menu-button' +
              (filterType == 10 ? ' MyRequest_menu-button-selected' : '')
            }
          >
            <div className='MyRequest_menu-button-underline' />
            <span>모집중</span>
          </a>
          <a
            onClick={onSelectedFilterType}
            data-type='20'
            className={
              'MyRequest_menu-button' +
              (filterType == 20 ? ' MyRequest_menu-button-selected' : '')
            }
          >
            <div className='MyRequest_menu-button-underline' />
            <span>거래중</span>
          </a>
          <a
            onClick={onSelectedFilterType}
            data-type='30'
            className={
              'MyRequest_menu-button' +
              (filterType == 30 ? ' MyRequest_menu-button-selected' : '')
            }
          >
            <div className='MyRequest_menu-button-underline' />
            <span>완료</span>
          </a>
        </div>
      </div>
      <div className='MyRequest_list'>
        {requestList.map((v, idx) => (
          <RequestItem
            requestInfo={v}
            key={idx}
            onBlock={onBlock}
            onChangeRequestInfo={onChangeRequestInfo}
            onDelete={onDelete}
            onHide={onHide}
            onRefresh={onRefresh}
          />
        ))}
        <div ref={bottomRef} className='MyRequest_bottom'>
          {isLoading && <Loader />}
        </div>
      </div>
    </div>
  );
}

export default MyRequest;
