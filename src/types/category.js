export default [
  {
    code: 30,
    value: 'errand',
    label: '심부름',
  },
  {
    code: 10,
    value: 'housework',
    label: '집안일',
  },
  {
    code: 20,
    value: 'engineering',
    label: '설치・조립・운반',
  },
  {
    code: 40,
    value: 'care',
    label: '요양',
  },
  {
    code: 50,
    value: 'hunting',
    label: '벌레 잡기',
  },

  {
    code: 60,
    value: 'agency',
    label: '대행',
  },
  {
    code: 70,
    value: 'mentoring',
    label: '과외・멘토링',
  },

  {
    code: 80,
    value: 'counsel',
    label: '상담',
  },

  {
    code: 0,
    value: 'etc',
    label: '기타',
  },
];
