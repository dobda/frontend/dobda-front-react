import {
  TOGGLE_CHATROOM_LIST,
  OPEN_CHATROOM_LIST,
  CLOSE_CHATROOM_LIST,
  SET_CHATROOM_INFO,
  RESET_ALL_CHAT_INFO,
  CREATE_CHATTING_ROOM,
} from './types';
import * as axios from '../../axios/axios_actions';

export function toggleChatRoomList() {
  return {
    type: TOGGLE_CHATROOM_LIST,
  };
}

export function closeChatRoomList() {
  return {
    type: CLOSE_CHATROOM_LIST,
  };
}

export function openChatRoomList() {
  return {
    type: OPEN_CHATROOM_LIST,
  };
}

export function setChatRoomInfo(chatRoomInfo) {
  return {
    type: SET_CHATROOM_INFO,
    data: chatRoomInfo,
  };
}

export function resetAllChatInfo() {
  return {
    type: RESET_ALL_CHAT_INFO,
  };
}

export function createChatRoom(partnerId) {
  const result = axios.createChatRoom(partnerId);

  return {
    type: CREATE_CHATTING_ROOM,
    payload: result,
  };
}
