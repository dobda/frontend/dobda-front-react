import './HelperApply.css';
import React, { useEffect, useState, useCallback } from 'react';
import HelperApproval from '../manager/HelperApproval';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { getHelperApplicants } from '../../axios/axios_actions';

function HelperApply() {
  const [applyList, setApplyList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await getHelperApplicants(page);
      setApplyList((applyList) => [...applyList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const removeItem = useCallback((applyInfo) => {
    if (!applyInfo) {
      return;
    }
    setApplyList((applyList) =>
      applyList.filter((v) => v.requestId != applyInfo.requestId)
    );
  }, []);

  return (
    <div className='HelperApply_root'>
      <div className='HelperApply_list'>
        {applyList.map((v, idx) => (
          <HelperApproval
            key={idx}
            applyInfo={v}
            onApprove={removeItem}
            onReject={removeItem}
          />
        ))}
        <div ref={bottomRef} className='HelperApply_bottom'>
          {isLoading && <Loader />}
        </div>
      </div>
    </div>
  );
}

export default HelperApply;
