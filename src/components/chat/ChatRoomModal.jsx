import './ChatRoomModal.css';

import React, { useCallback, useEffect } from 'react';
import { createPortal } from 'react-dom';
import ChatRoom from './ChatRoom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { setChatRoomInfo } from '../../_redux/actions/modal_action';
import { useSelector, useDispatch } from 'react-redux';

function ChatRoomModal() {
  const dispatch = useDispatch();

  const isShowChatRoomList = useSelector(
    (state) => state.modal.isShowChatRoomList
  );
  const chatRoomInfo = useSelector((state) => state.modal.chatRoomInfo);

  useEffect(() => {
    return () => {
      document.body.style.overflow = 'unset';
    };
  });

  const close = useCallback((event) => {
    event.preventDefault();
    dispatch(setChatRoomInfo());
  }, []);

  return createPortal(
    <div
      className={
        'ChatRoomModal_root' +
        (isShowChatRoomList ? ' ChatRoomModal_root_ms' : '')
      }
      onMouseEnter={() => (document.body.style.overflow = 'hidden')}
      onMouseLeave={() => (document.body.style.overflow = 'unset')}
    >
      <div className='ChatRoomModal_bar'>
        <span>{chatRoomInfo?.partnerNickname}</span>
        <div onClick={close}>
          <FontAwesomeIcon icon={faXmark} />
        </div>
      </div>
      <div className='ChatRoomModal_list'>
        <ChatRoom />
      </div>
    </div>,
    document.getElementById('chatRoomModal')
  );
}

export default ChatRoomModal;
