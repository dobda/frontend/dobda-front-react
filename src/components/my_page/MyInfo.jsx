import './MyInfo.css';
import React, { useState, useCallback, useMemo, useRef } from 'react';
import { useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { myInfo } from '../../_redux/actions/user_actions';
import UnderlineInput from '../common/UnderlineInput';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Button } from 'react-bootstrap';
import {
  faPenToSquare,
  faCircleCheck,
  faCircleXmark,
  faSms,
} from '@fortawesome/free-solid-svg-icons';
import {
  changeEmail,
  changePwd,
  changePhoneNum,
  changeNickname,
  changeIntroduction,
  requestReAuthSms,
  checkReAuthSms,
  changeAddress,
  changeBanking,
  leaveMember,
} from '../../axios/axios_actions';
import AccountListModal from '../payment/AccountListModal';
import DaumPostModal from '../common/DaumPostModal';

function MyInfo() {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const userInfo = useSelector((state) => state.user.userInfo);

  const [email, setEmail] = useState('');
  const [pwd, setPwd] = useState('');
  const [modPwd, setModPwd] = useState('');
  const [nickname, setNickname] = useState('');
  const [phoneNum, setPhoneNum] = useState('');
  const [phonePwd, setPhonePwd] = useState('');
  const [introduction, setIntroduction] = useState('');
  const [address, setAddress] = useState('');
  const [isModEamil, setIsModEamil] = useState(false);
  const [isModPwd, setIsModPwd] = useState(false);
  const [isModNickname, setIsModNickname] = useState(false);
  const [isModPhoneNum, setIsModPhoneNum] = useState(false);
  const [isValidKeyNum, setIsValidKeyNum] = useState(false);
  const [isSendKeyNum, setIsSendKeyNum] = useState(false);
  const [isModAddress, setIsModAddress] = useState(false);
  const [openAccountListModal, setOpenAccountListModal] = useState(false);
  const [openDaumModal, setOpenDaumModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [keyNum, setKeyNum] = useState('');

  const delpwdinput = useRef();

  const [name, trustPoint] = useMemo(() => {
    if (!userInfo) {
      return [];
    }
    setEmail(userInfo.email);
    setNickname(userInfo.nickname);
    setPhoneNum(userInfo.phoneNum);
    setIntroduction(userInfo.introduction);
    if (userInfo.helperInfo != null) {
      setAddress(userInfo.helperInfo.address);
    }
    return [userInfo.name, Math.round(userInfo.trustPoint * 10) / 10.0];
  }, [userInfo]);

  const onChangeEmail = useCallback((e) => {
    setEmail(e.target.value);
  });

  const onChangePwd = useCallback((e) => {
    setPwd(e.target.value);
  });

  const onChangeModPwd = useCallback((e) => {
    setModPwd(e.target.value);
  });

  const onChangeNickname = useCallback((e) => {
    setNickname(e.target.value);
  });

  const onChangePhoneNum = useCallback((e) => {
    setPhoneNum(e.target.value);
  });

  const onChangePhonePwd = useCallback((e) => {
    setPhonePwd(e.target.value);
  });

  const onChangeKeyNum = useCallback((e) => {
    setKeyNum(e.target.value);
  });

  const onChangeIntroduction = useCallback((e) => {
    setIntroduction(e.target.value);
  });

  const cancelEditEmail = useCallback(() => {
    setEmail(userInfo.email);
    setIsModEamil(false);
  }, [userInfo]);

  const confirmEditEmail = useCallback(() => {
    if (userInfo?.email == email || !email) {
      alert('변경할 이메일을 입력해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 이메일을 변경하시겠습니까?')) {
      return;
    }

    changeEmail(email)
      .then(() => {
        alert('이메일이 변경되었습니다.😊');
        setIsModEamil(false);
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [userInfo, email]);

  const cancelEditPwd = useCallback(() => {
    setPwd('');
    setModPwd('');
    setIsModPwd(false);
  }, [userInfo]);

  const cancelEditNickname = useCallback(() => {
    setNickname(userInfo.nickname);
    setIsModNickname(false);
  }, [userInfo]);

  const onClickConfirmPwd = useCallback(() => {
    if (!pwd || !modPwd) {
      alert('비밀번호를 입력해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 비밀번호를 변경하시겠습니까?')) {
      return;
    }

    changePwd(pwd, modPwd)
      .then(() => {
        alert('비밀번호가 변경되었습니다.😊');
        setPwd('');
        setModPwd('');
        setIsModPwd(false);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [pwd, modPwd]);

  const confirmEditNickname = useCallback(() => {
    if (userInfo?.nickname == nickname || !nickname) {
      alert('변경할 닉네임을 입력해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 닉네임을 변경하시겠습니까?')) {
      return;
    }

    changeNickname(nickname)
      .then(() => {
        alert('닉네임이 변경되었습니다.😊');
        setIsModNickname(false);
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [nickname, userInfo]);

  const cancelEditPhoneNum = useCallback(() => {
    setPhoneNum(userInfo.phoneNum);
    setIsModPhoneNum(false);
    setIsSendKeyNum(false);
    setIsValidKeyNum(false);
    setKeyNum('');
    setPhonePwd('');
  }, [userInfo]);

  const onClickRequestKey = useCallback(() => {
    if (phoneNum == userInfo.phoneNum || phoneNum == '') {
      return alert('변경할 전화번호를 입력해주세요.😥');
    }
    requestReAuthSms(phoneNum)
      .then(({ data }) => {
        alert(
          `인증번호가 발송되었습니다. 5분 내에 인증해주세요.😊\n테스트용이기 때문에 실제 sms는 발송하지 않고 있습니다.\n인증코드 ${data}를 입력해주세요`
        );
        setIsSendKeyNum(true);
        setIsValidKeyNum(false);
        setKeyNum('');
        setPhonePwd('');
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [phoneNum, userInfo]);

  const onClickAuthKeyNum = useCallback(() => {
    if (keyNum == '') {
      return alert('인증번호를 입력해주세요.😥');
    }

    checkReAuthSms(phoneNum, keyNum)
      .then(() => {
        alert('인증이 성공했습니다.😊');
        setIsValidKeyNum(true);
        setPhonePwd('');
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [phoneNum, keyNum]);

  const onClickChangePhoneNum = useCallback(() => {
    if (phonePwd == '') {
      return alert('비밀번호를 입력해주세요.😥');
    }

    changePhoneNum(phoneNum, keyNum, phonePwd)
      .then(() => {
        alert('전화번호가 변경되었습니다.😊');
        cancelEditPhoneNum();
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [phoneNum, keyNum, phonePwd, cancelEditPhoneNum]);

  const confirmEditIntroduction = useCallback(() => {
    if (userInfo?.introduction == introduction) {
      alert('자기소개 내용을 변경해주세요.😥');
      return;
    }

    if (!window.confirm('정말로 자기소개를 변경하시겠습니까?')) {
      return;
    }

    changeIntroduction(introduction)
      .then(() => {
        alert('자기소개가 변경되었습니다.😊');
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [introduction]);

  const cancelEditAddress = useCallback(() => {
    setAddress(userInfo?.helperInfo?.address);
    setIsModAddress(false);
  }, [userInfo]);

  const onReturnAddress = useCallback((data) => {
    setAddress(data.address);
  }, []);

  const confirmEditAddress = useCallback(() => {
    if (!window.confirm('정말로 주소를 변경하시겠습니까?')) {
      return;
    }

    changeAddress(address)
      .then(() => {
        alert('주소가 변경되었습니다.😊');
        setIsModAddress(false);
        dispatch(myInfo());
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [address]);

  const onSelectAccount = useCallback((accountInfo) => {
    changeBanking(accountInfo?.accountHolderName, accountInfo?.fintechNum)
      .then(() => {
        alert('입금 계좌가 변경되었습니다.😊');
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, []);

  const deleteInProgress = (e) => {
    if (delpwdinput.current.value === '') {
      alert('비밀번호를 입력해주세요.');
      return;
    }

    leaveMember(delpwdinput.current.value)
      .then(() => {
        navigate('/');
        setShowDeleteModal(false);
      })
      .catch((err) => {
        if (err.response && err.response.status == 401) {
          alert('비밀번호가 일치하지 않습니다.');
        } else if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  };

  return (
    <div className='MyInfo_root'>
      <div>
        <span className='MyInfo_title'>개인정보</span>
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>이메일</span>
          {isModEamil ? (
            <>
              <div className='MyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={confirmEditEmail}
                />
              </div>
              <div className='MyInfo_subtitle-button' onClick={cancelEditEmail}>
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='MyInfo_subtitle-button'
              onClick={() => {
                setIsModEamil(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        <div>
          <UnderlineInput
            value={email}
            onChange={onChangeEmail}
            readOnly={!isModEamil}
          />
        </div>
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>비밀번호 변경</span>
          {isModPwd ? (
            <>
              <div className='MyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={onClickConfirmPwd}
                />
              </div>
              <div className='MyInfo_subtitle-button' onClick={cancelEditPwd}>
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='MyInfo_subtitle-button'
              onClick={() => {
                setIsModPwd(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        {isModPwd ? (
          <>
            <div>
              <UnderlineInput
                type='password'
                value={pwd}
                onChange={onChangePwd}
                placeholder='기존 비밀번호를 입력해주세요.'
              />
            </div>
            <div style={{ marginTop: '15px' }}>
              <UnderlineInput
                type='password'
                value={modPwd}
                placeholder='변경할 비밀번호를 입력해주세요.'
                onChange={onChangeModPwd}
              />
            </div>
          </>
        ) : null}
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>실명</span>
        </div>
        <div>
          <UnderlineInput value={name} readOnly />
        </div>
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>닉네임</span>
          {isModNickname ? (
            <>
              <div className='MyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={confirmEditNickname}
                />
              </div>
              <div
                className='MyInfo_subtitle-button'
                onClick={cancelEditNickname}
              >
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='MyInfo_subtitle-button'
              onClick={() => {
                setIsModNickname(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        <div>
          <UnderlineInput
            value={nickname}
            onChange={onChangeNickname}
            readOnly={!isModNickname}
          />
        </div>
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>전화번호</span>
          {isModPhoneNum ? (
            <>
              <div className='MyInfo_subtitle-button'>
                <FontAwesomeIcon icon={faSms} onClick={onClickRequestKey} />
              </div>
              <div
                className='MyInfo_subtitle-button'
                onClick={cancelEditPhoneNum}
              >
                <FontAwesomeIcon icon={faCircleXmark} />
              </div>
            </>
          ) : (
            <div
              className='MyInfo_subtitle-button'
              onClick={() => {
                setIsModPhoneNum(true);
              }}
            >
              <FontAwesomeIcon icon={faPenToSquare} />
            </div>
          )}
        </div>
        <div>
          <UnderlineInput
            value={phoneNum}
            onChange={onChangePhoneNum}
            readOnly={!isModPhoneNum || isSendKeyNum}
          />
        </div>
        {isSendKeyNum && !isValidKeyNum && (
          <>
            <div className='MyInfo_subtitle'>
              <span>인증번호 확인</span>
              <div className='MyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={onClickAuthKeyNum}
                />
              </div>
            </div>
            <div>
              <UnderlineInput
                value={keyNum}
                onChange={onChangeKeyNum}
                placeholder='받은 인증번호를 입력해주세요.'
                readOnly={false}
              />
            </div>
          </>
        )}
        {isValidKeyNum && (
          <>
            <div className='MyInfo_subtitle'>
              <span>비밀번호 확인</span>
              <div className='MyInfo_subtitle-button'>
                <FontAwesomeIcon
                  icon={faCircleCheck}
                  onClick={onClickChangePhoneNum}
                />
              </div>
            </div>
            <div>
              <UnderlineInput
                value={phonePwd}
                type='password'
                onChange={onChangePhonePwd}
                placeholder='비밀번호를 입력해주세요'
                readOnly={false}
              />
            </div>
          </>
        )}
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>나의 신뢰 점수</span>
        </div>
        <div>
          <UnderlineInput value={trustPoint} readOnly />
        </div>
      </div>
      <div>
        <div className='MyInfo_subtitle'>
          <span>자기소개</span>
          <div className='MyInfo_subtitle-button'>
            <FontAwesomeIcon
              icon={faCircleCheck}
              onClick={confirmEditIntroduction}
            />
          </div>
        </div>
        <div>
          <UnderlineInput
            value={introduction}
            onChange={onChangeIntroduction}
            readOnly={false}
          />
        </div>
      </div>
      {userInfo?.isHelper && (
        <>
          <div>
            <span className='MyInfo_title'>헬퍼</span>
          </div>
          <div>
            <div className='MyInfo_subtitle'>
              <span>주소</span>
              {isModAddress ? (
                <>
                  <div className='MyInfo_subtitle-button'>
                    <FontAwesomeIcon
                      icon={faCircleCheck}
                      onClick={confirmEditAddress}
                    />
                  </div>
                  <div
                    className='MyInfo_subtitle-button'
                    onClick={cancelEditAddress}
                  >
                    <FontAwesomeIcon icon={faCircleXmark} />
                  </div>
                </>
              ) : (
                <div
                  className='MyInfo_subtitle-button'
                  onClick={() => {
                    setIsModAddress(true);
                  }}
                >
                  <FontAwesomeIcon icon={faPenToSquare} />
                </div>
              )}
            </div>
            <div onClick={() => isModAddress && setOpenDaumModal(true)}>
              <UnderlineInput value={address} readOnly />
              {openDaumModal && (
                <DaumPostModal
                  onClose={() => setOpenDaumModal(false)}
                  onComplete={onReturnAddress}
                  closeWhenClickBack
                />
              )}
            </div>
          </div>
          <div>
            <div className='MyInfo_subtitle'>
              <span>입금 계좌 변경</span>
              <div
                className='MyInfo_subtitle-button'
                onClick={() => setOpenAccountListModal(true)}
              >
                <FontAwesomeIcon icon={faPenToSquare} />
              </div>
              {openAccountListModal && (
                <AccountListModal
                  onClose={() => setOpenAccountListModal(false)}
                  onSelectAccount={onSelectAccount}
                />
              )}
            </div>
          </div>
        </>
      )}
      <div className='MyInfo_leave-btn-box'>
        <Button variant='danger' onClick={() => setShowDeleteModal(true)}>
          회원 탈퇴
        </Button>
        {showDeleteModal ? (
          <div className='delete-modal'>
            <div className='ddmm01'>
              <p>
                탈퇴를 누르시면 모든 정보가 사라집니다.
                <br />
                탈퇴 하시겠습니까?
              </p>
              <input
                ref={delpwdinput}
                type='password'
                name='pwd'
                placeholder='비밀번호를 입력해주세요.'
              />
            </div>
            <div className='ddmm02'>
              <button
                className='ddmm02n'
                onClick={() => {
                  setShowDeleteModal(!showDeleteModal);
                }}
              >
                취소
              </button>
              <button className='ddmm02y' onClick={deleteInProgress}>
                탈퇴
              </button>
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default MyInfo;
