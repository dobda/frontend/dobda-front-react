/* global kakao */
import './RequestResults.css';

import React, { useEffect, useCallback, useState, useMemo } from 'react';
import RequestItem from './RequestItem';
import { useSearchParams, useNavigate, Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { searchRequestList } from '../../axios/axios_actions';
import { saveMapCenter, saveMapZoom } from '../../_redux/actions/map_actions';
import requestStateList from '../../types/requestStateList';
import categoryList from '../../types/category';
// import useCurrentLocation from '../../util/useCurrentPosition';

import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import { Dropdown } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAngleDoubleLeft,
  faAngleDoubleRight,
  faAngleLeft,
  faAngleRight,
} from '@fortawesome/free-solid-svg-icons';

const { kakao } = window;

function RequestResults() {
  const [searchParams] = useSearchParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const initCenter = useSelector((state) => state.map.center);
  const initZoom = useSelector((state) => state.map.zoom);

  // const { location: currentLocation, error: currentError } = useCurrentLocation(
  //   { enableHighAccuracy: true }
  // );
  const [kakaoMap, setKakaoMap] = useState();
  const [markerList, setMarkerList] = useState([]);
  const [requestList, setRequestList] = useState([]);
  const [infoWindow, setInfoWindow] = useState();
  const [isShowList, setIsShowList] = useState(true);
  const [bounds, setBounds] = useState();
  const [totalPage, setTotalPage] = useState(1);
  const [page, setPage] = useState(1);
  const [startPageNum, setStartPageNum] = useState(1);
  const [anchorEl, setAnchorEl] = useState(null);
  const [onlyWaiting, setOnlyWaiting] = useState(false);
  const [sort, setSort] = useState('rc');
  const open = Boolean(anchorEl);

  const [categories, minPrice, maxPrice, keyword] = useMemo(() => {
    const categories = searchParams.getAll('c');
    const minPrice = searchParams.get('mp');
    const maxPrice = searchParams.get('xp');
    const keyword = searchParams.get('qs');

    return [categories, minPrice, maxPrice, keyword];
  }, [searchParams]);

  // useEffect(() => {
  //   console.log(currentLocation);
  //   console.log(currentError);
  // }, [currentLocation, currentError]);

  useEffect(() => {
    setStartPageNum(page - ((page - 1) % 5));
  }, [page]);

  useEffect(() => {
    const container = document.getElementById('RequestResults_map');
    const options = {
      center: new kakao.maps.LatLng(37.566134, 126.977808),
      level: 3,
    };

    const kakaoMap = new kakao.maps.Map(container, options);
    if (initCenter) {
      kakaoMap.setCenter(new kakao.maps.LatLng(initCenter.Ma, initCenter.La));
    }
    if (initZoom) {
      kakaoMap.setLevel(initZoom);
    }
    setKakaoMap(kakaoMap);
    setBounds(kakaoMap.getBounds());

    const infoWindow = new kakao.maps.InfoWindow({
      removable: true,
      altitude: 30,
    });
    setInfoWindow(infoWindow);

    kakao.maps.event.addListener(kakaoMap, 'center_changed', function () {});

    kakao.maps.event.addListener(kakaoMap, 'click', function (mouseEvent) {
      // var latlng = mouseEvent.latLng;
      // alert('click! ' + latlng.toString());
      infoWindow.close();
    });

    kakao.maps.event.addListener(kakaoMap, 'dragend', function () {
      setBounds(kakaoMap.getBounds());
      dispatch(saveMapCenter(kakaoMap.getCenter()));
    });

    kakao.maps.event.addListener(kakaoMap, 'zoom_changed', function () {
      setBounds(kakaoMap.getBounds());
      dispatch(saveMapZoom(kakaoMap.getLevel()));
    });

    // kakao.maps.event.addListener(kakaoMap, 'bounds_changed', function () {
    //   let para = document.querySelector('.RequestResults_map-tester');
    //   if (window.getComputedStyle(para).getPropertyValue('display') == 'none') {
    //     return;
    //   }
    //   setBounds(kakaoMap.getBounds());
    //   changePage(1);
    // });
  }, []);

  useEffect(() => {
    if (!bounds) {
      return;
    }

    const swLatlng = bounds.getSouthWest(); // 남서쪽, 작은 좌표
    const neLatlng = bounds.getNorthEast(); // 북동쪽, 큰 좌표
    searchRequestList(
      page,
      sort,
      keyword,
      onlyWaiting,
      categories,
      minPrice,
      maxPrice,
      swLatlng.La,
      swLatlng.Ma,
      neLatlng.La,
      neLatlng.Ma
    )
      .then((response) => {
        setRequestList(response.data.content);
        setTotalPage(
          response.data.totalPages == 0 ? 1 : response.data.totalPages
        );
      })
      .catch((err) => {
        setRequestList([]);
        if (!err.response) {
          alert('서버가 응답하지 않습니다.');
        }
      });
  }, [
    bounds,
    categories,
    minPrice,
    maxPrice,
    page,
    keyword,
    onlyWaiting,
    sort,
  ]);

  useEffect(() => {
    const newMarkerList = [];
    requestList.forEach((request) => {
      const position = new kakao.maps.LatLng(request.axisY, request.axisX);

      // marker 생성
      const markerContent = document.createElement('div');
      markerContent.className = 'RequestResults_marker';

      const markerSpan = document.createElement('span');
      markerSpan.appendChild(
        document.createTextNode('₩' + request.payment.toLocaleString('ko-KR'))
      );
      markerContent.appendChild(markerSpan);

      const marker = new kakao.maps.CustomOverlay({
        position: position,
        content: markerContent,
      });
      marker.setMap(kakaoMap);
      newMarkerList.push(marker);

      markerContent.onclick = () => {
        infoWindow.close();

        // infoWindow 새로 생성
        const infoWindowContent = document.createElement('div');
        infoWindowContent.className = 'RequestResults_iw';
        const iwContent = document.createElement('div');
        iwContent.className = 'RequestResults_iw-content';

        const iwTitleBox = document.createElement('a');
        iwTitleBox.className = 'RequestResults_iw-title-box';
        const iwState = document.createElement('span');
        iwState.className = 'RequestResults_iw-state';
        const iwTitle = document.createElement('span');
        iwTitle.className = 'RequestResults_iw-title';

        const iwCondition = document.createElement('div');
        iwCondition.className = 'RequestResults_iw-condition';
        const iwSuggest = document.createElement('span');
        iwSuggest.className = request?.isCanSuggestPayment
          ? 'RequestResults_iw-suggest-O'
          : 'RequestResults_iw-suggest-N';
        const iwPrice = document.createElement('span');
        iwPrice.className = 'RequestResults_iw-price';
        const iwCategory = document.createElement('span');
        iwCategory.className = 'RequestResults_iw-category';

        iwTitleBox.appendChild(iwState);
        iwTitleBox.appendChild(iwTitle);
        iwCondition.appendChild(iwSuggest);
        iwCondition.appendChild(iwPrice);
        iwCondition.appendChild(iwCategory);
        iwContent.appendChild(iwTitleBox);
        iwContent.appendChild(iwCondition);
        infoWindowContent.append(iwContent);

        let state = requestStateList.find(
          (v) => v.code == request.state
        )?.label;
        let category = categoryList.find(
          (v) => v.code == request.category
        )?.label;

        iwState.appendChild(document.createTextNode(state ?? ''));
        iwTitle.appendChild(document.createTextNode(request?.title ?? ''));
        iwSuggest.appendChild(
          document.createTextNode(
            request?.isCanSuggestPayment ? '초과 가능' : '초과 불가'
          )
        );
        iwPrice.appendChild(
          document.createTextNode(
            '₩' + (request?.payment.toLocaleString('ko-KR') ?? '')
          )
        );
        iwCategory.appendChild(document.createTextNode(category ?? ''));

        const link = `/view?id=${request.requestId}`;
        iwTitleBox.href = link;
        iwTitleBox.onclick = (event) => {
          event.preventDefault();
          navigate(link);
        };

        infoWindow.setPosition(position);
        infoWindow.setContent(infoWindowContent);

        infoWindow.open(kakaoMap);
      };
    });

    setMarkerList((markerList) => {
      markerList.forEach((marker) => {
        marker.setMap(null);
      });
      return newMarkerList;
    });
  }, [requestList, kakaoMap]);

  const toggleShowList = useCallback(() => {
    setIsShowList((isShowList) => !isShowList);
    new Promise((resolve) => setTimeout(resolve, 10)).then(() =>
      kakaoMap.relayout()
    );
  }, [kakaoMap]);

  const changePage = useCallback(
    (pageNum) => {
      if (pageNum < 1) {
        pageNum = 1;
      }
      if (totalPage != 0 && pageNum > totalPage) {
        pageNum = totalPage;
      }
      setPage(pageNum);
    },
    [totalPage]
  );

  useEffect(() => {
    changePage(1);
  }, [bounds, changePage]);

  const onChangeRequestInfo = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.map((v) =>
        v.requestId == requestInfo.requestId ? { ...requestInfo } : v
      )
    );
  }, []);

  const onDelete = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  const onRefresh = useCallback(
    (requestInfo) => {
      if (!requestInfo) {
        return;
      }
      setRequestList((requestList) => {
        const newList = requestList.filter(
          (v) => v.requestId != requestInfo.requestId
        );
        if (page <= 1) {
          newList.unshift(requestInfo);
        }
        return newList;
      });
    },
    [page]
  );

  const onHide = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  const onBlock = useCallback((requestInfo) => {
    if (!requestInfo) {
      return;
    }
    setRequestList((requestList) =>
      requestList.filter((v) => v.requestId != requestInfo.requestId)
    );
  }, []);

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  return (
    <main className='RequestResults_root'>
      <div className='RequestResults_results'>
        <div
          className={
            'RequestResults_left' + (isShowList ? '' : ' RequestResults_hidden')
          }
        >
          {requestList.map((v, idx) => (
            <RequestItem
              requestInfo={v}
              key={idx}
              onBlock={onBlock}
              onChangeRequestInfo={onChangeRequestInfo}
              onDelete={onDelete}
              onHide={onHide}
              onRefresh={onRefresh}
            />
          ))}
        </div>
        <div
          className={
            'RequestResults_right RequestResults_map-tester' +
            (isShowList ? ' RequestResults_hidden' : '')
          }
        >
          <div className='RequestResults_right_sticky'>
            <div id='RequestResults_map' className='RequestResults_map'></div>
          </div>
        </div>
        <div className='RequestResults_toggle' onClick={toggleShowList}>
          <span>{isShowList ? '지도 표시하기' : '목록 보기'}</span>
        </div>
      </div>
      <div className='RequestResults_results RequestResults_bottom-sticky'>
        <div
          className={
            'RequestResults_bottom-left' +
            (isShowList ? '' : ' RequestResults_hidden')
          }
        >
          <div className='RequestResults_page-bar'>
            {totalPage > 0 && (
              <div className='RequestResults_prd-paging'>
                <div
                  className='RequestResults_pageNum'
                  onClick={() => changePage(1)}
                >
                  <div className='RequestResults_pageNum-box'>
                    <FontAwesomeIcon
                      icon={faAngleDoubleLeft}
                      className='RequestResults_to-left-end'
                    />
                  </div>
                </div>
                <div
                  className='RequestResults_pageNum'
                  onClick={() => changePage(startPageNum - 1)}
                >
                  <div className='RequestResults_pageNum-box'>
                    <FontAwesomeIcon
                      icon={faAngleLeft}
                      className='RequestResults_to-left'
                    />
                  </div>
                </div>
                {[...Array(5)].map((e, i) => {
                  const num = startPageNum + i;
                  if (num > totalPage) {
                    return;
                  }
                  return (
                    <div
                      className={
                        'RequestResults_pageNum' +
                        (num == page ? ' RequestResults_selected' : '')
                      }
                      key={i}
                      onClick={() => changePage(num)}
                    >
                      <div className='RequestResults_pageNum-box'>
                        <span>{num}</span>
                      </div>
                    </div>
                  );
                })}
                <div
                  className='RequestResults_pageNum'
                  onClick={() => changePage(startPageNum + 5)}
                >
                  <div className='RequestResults_pageNum-box'>
                    <FontAwesomeIcon
                      icon={faAngleRight}
                      className='RequestResults_to-right'
                    />
                  </div>
                </div>
                <div
                  className='RequestResults_pageNum'
                  onClick={() => changePage(totalPage)}
                >
                  <div className='RequestResults_pageNum-box'>
                    <FontAwesomeIcon
                      icon={faAngleDoubleRight}
                      className='RequestResults_to-left-end'
                    />
                  </div>
                </div>
              </div>
            )}
            <div className='RequestResults_page-menu'>
              <IconButton
                aria-label='more'
                className='RequestResults_long-button'
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup='ture'
                onClick={handleClick}
              >
                <MoreVertIcon />
              </IconButton>
              <Menu
                MenuListProps={{ 'aria-labelledby': 'long-button' }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
              >
                <MenuItem onClick={() => setSort('rc')}>
                  최신순{sort == 'rc' ? ' *' : ''}
                </MenuItem>
                <MenuItem onClick={() => setSort('dp')}>
                  가격 내림차순{sort == 'dp' ? ' *' : ''}
                </MenuItem>
                <MenuItem onClick={() => setSort('up')}>
                  가격 오름차순{sort == 'up' ? ' *' : ''}
                </MenuItem>
                <Dropdown.Divider />
                {onlyWaiting ? (
                  <MenuItem onClick={() => setOnlyWaiting(false)}>
                    전부 보기
                  </MenuItem>
                ) : (
                  <MenuItem onClick={() => setOnlyWaiting(true)}>
                    모집중인 글만 보기
                  </MenuItem>
                )}
              </Menu>
            </div>
          </div>
        </div>
        <div
          className={
            'RequestResults_bottom-right' +
            (isShowList ? ' RequestResults_hidden' : '')
          }
        >
          <Link to='/write' className='RequestResults_right-add-button'>
            <span>+</span>
          </Link>
        </div>
      </div>
    </main>
  );
}

export default RequestResults;
