import './MyOrder.css';
import React, { useEffect, useState, useCallback } from 'react';
import OrderItem from '../order/OrderItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import {
  getMyReqestOrderList,
  retryRefundAll,
} from '../../axios/axios_actions';

function MyOrder() {
  const [filterType, setFilterType] = useState(0);
  const [orderList, setOrderList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    setOrderList([]);
    setIsEnd(false);
    setPage(0);
  }, [filterType]);

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const filters = [filterType];
      if (filterType == 40) {
        filters.push(70);
      }
      const response = await getMyReqestOrderList(page, filters);
      setOrderList((orderList) => [...orderList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page, filterType]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  const onSelectedFilterType = useCallback(
    (event) => {
      event.preventDefault();
      if (isLoading) {
        return;
      }
      setFilterType(event.currentTarget.dataset.type);
    },
    [isLoading]
  );

  const onChangeOrder = useCallback((orderInfo) => {
    if (!orderInfo) {
      return;
    }
    setOrderList((orderList) => orderList.filter((v) => v.id != orderInfo.id));
  }, []);

  const onConfirmReview = useCallback((reviewInfo) => {
    if (!reviewInfo) {
      return;
    }
    setOrderList((orderList) =>
      orderList.map((v) => {
        if (v.id == reviewInfo.orderId) {
          const newOrder = { ...v };
          if (newOrder.reviews == null) {
            newOrder.reviews = [];
          }
          newOrder.reviews.push(reviewInfo);
          return newOrder;
        }
        return v;
      })
    );
  }, []);

  const onSelectAccount = useCallback((accountInfo) => {
    if (!window.confirm('선택한 계좌로 환불 절차를 신청하시겠습니까?')) {
      return;
    }

    retryRefundAll(accountInfo?.accountHolderName, accountInfo?.fintechNum)
      .then(() => {
        alert('선택된 계좌로 환불 요청을 성공했습니다.\n새로고침을 해주세요.');
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, []);

  return (
    <div className='MyOrder_root'>
      <div className='MyOrder_menu-root'>
        <div className='MyOrder_menu'>
          <div className='MyOrder_menu-underline' />
          <a
            onClick={onSelectedFilterType}
            data-type='0'
            className={
              'MyOrder_menu-button' +
              (filterType == 0 ? ' MyOrder_menu-button-selected' : '')
            }
          >
            <div className='MyOrder_menu-button-underline' />
            <span>수락 대기 중</span>
          </a>
          <a
            onClick={onSelectedFilterType}
            data-type='10'
            className={
              'MyOrder_menu-button' +
              (filterType == 10 ? ' MyOrder_menu-button-selected' : '')
            }
          >
            <div className='MyOrder_menu-button-underline' />
            <span>수락됨</span>
          </a>
          <a
            onClick={onSelectedFilterType}
            data-type='40'
            className={
              'MyOrder_menu-button' +
              (filterType == 40 ? ' MyOrder_menu-button-selected' : '')
            }
          >
            <div className='MyOrder_menu-button-underline' />
            <span>거래완료</span>
          </a>
          <a
            onClick={onSelectedFilterType}
            data-type='60'
            className={
              'MyOrder_menu-button' +
              (filterType == 60 ? ' MyOrder_menu-button-selected' : '')
            }
          >
            <div className='MyOrder_menu-button-underline' />
            <span>환불 실패</span>
          </a>
        </div>
      </div>
      <div className='MyOrder_list'>
        {orderList.map((v, idx) => (
          <OrderItem
            orderInfo={v}
            key={idx}
            isHelperProfile
            onApprove={onChangeOrder}
            onCancel={onChangeOrder}
            onComplete={onChangeOrder}
            onRetry={onChangeOrder}
            onSelectAccount={onSelectAccount}
            onConfirmReview={onConfirmReview}
          />
        ))}
        <div ref={bottomRef} className='MyOrder_bottom'>
          {isLoading && <Loader />}
        </div>
      </div>
    </div>
  );
}

export default MyOrder;
