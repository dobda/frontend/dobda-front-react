import { MAP_CENTER, MAP_ZOOM } from './types';

export function saveMapCenter(data) {
  return {
    type: MAP_CENTER,
    data,
  };
}

export function saveMapZoom(data) {
  return {
    type: MAP_ZOOM,
    data,
  };
}
