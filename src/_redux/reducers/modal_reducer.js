import {
  TOGGLE_CHATROOM_LIST,
  OPEN_CHATROOM_LIST,
  CLOSE_CHATROOM_LIST,
  SET_CHATROOM_INFO,
  RESET_ALL_CHAT_INFO,
  CREATE_CHATTING_ROOM_OK,
} from '../actions/types';

export default function (state = {}, action) {
  switch (action.type) {
    case TOGGLE_CHATROOM_LIST:
      return {
        ...state,
        isShowChatRoomList: !(state.isShowChatRoomList ?? false),
      };
    case OPEN_CHATROOM_LIST:
      return { ...state, isShowChatRoomList: true };
    case CLOSE_CHATROOM_LIST:
      return { ...state, isShowChatRoomList: false };
    case SET_CHATROOM_INFO:
      return { ...state, chatRoomInfo: action.data };
    case RESET_ALL_CHAT_INFO:
      delete state.isShowChatRoomList;
      delete state.chatRoomInfo;
      return { ...state };
    case CREATE_CHATTING_ROOM_OK:
      return { ...state, chatRoomInfo: action.payload.data };
    default:
      return state;
  }
}
