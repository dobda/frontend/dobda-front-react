import React, { useCallback, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './ViewOffer.css';
import Chip from '@mui/material/Chip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Button from '@mui/material/Button';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import requestApplyStateList from '../../types/requestApplyStateList';
import { timeForToday } from '../../util/utils';
import { createChatRoom } from '../../_redux/actions/modal_action';
import {
  deleteRequestApply,
  rejectRequestApply,
} from '../../axios/axios_actions';
import View_OfferForm from '../write_layouts/View_OfferForm';
import PaymentModal from '../payment/PaymentModal';

export default function ViewOffer({
  applyInfo,
  onReject,
  onDelete,
  onConfirm,
  onApprove,
  isHelperProfile = true,
}) {
  const dispatch = useDispatch();

  const userInfo = useSelector((state) => state.user.userInfo);

  const [state, setState] = useState();
  const [requesterIsMe, setRequesterIsMe] = useState(false);
  const [helperIsMe, setHelperIsMe] = useState(false);
  const [openOfferModal, setOpenOfferModal] = useState(false);
  const [openPaymentModal, setPaymentModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (!applyInfo) {
      return;
    }
    setState(
      requestApplyStateList.find((v) => v.code == applyInfo.state)?.label
    );
    setHelperIsMe(
      !userInfo?.isAdmin && userInfo?.id == applyInfo.helperInfo?.id
    );
    setRequesterIsMe(
      !userInfo?.isAdmin && userInfo?.id == applyInfo.requestInfo.writerInfo?.id
    );
  }, [applyInfo]);

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const onClickMessage = useCallback(() => {
    if (!userInfo) {
      return alert('로그인을 해주세요');
    }

    const id = isHelperProfile
      ? applyInfo?.helperInfo?.id
      : applyInfo?.requestInfo.writerInfo?.id;

    if (id) {
      dispatch(createChatRoom(id));
    }
  }, [applyInfo, isHelperProfile]);

  const onClickDelete = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 삭제하시겠습니까?')) {
      return;
    }

    deleteRequestApply(applyInfo?.id)
      .then(() => {
        alert('제안을 삭제했습니다.');
        onDelete?.(applyInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyInfo]);

  const onClickApprove = useCallback(() => {
    setPaymentModal(true);
  }, [applyInfo]);

  const onApproveInternal = useCallback(() => {
    handleClose();
    if (!applyInfo) {
      return;
    }
    applyInfo.state = 30;
    onApprove?.(applyInfo);
  }, [applyInfo, onApprove]);

  const onClickReject = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 거절하시겠습니까?')) {
      return;
    }

    rejectRequestApply(applyInfo?.id)
      .then(() => {
        alert('제안을 거절했습니다.');
        onReject?.(applyInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyInfo]);

  return (
    <section id='Offer_article-comments'>
      <div id='Offer_article-comments-list'>
        <div class='Offer_article-comment'>
          <div class='Offer_article-comment-wrap'>
            <div class='Offer_article-comment-profile'>
              <div className='Offer_profile'>
                <div className='Offer_profile-image-box'>
                  <div className='Offer_rounding-profile-image'>
                    <img src='' onError={onFallbackImg} />
                  </div>
                </div>
                <div className='Offer_profile-detail-box'>
                  <div className='Offer_profile-name-box'>
                    <Button
                      className='Offer_article-comment-nickname'
                      aria-controls={open ? 'basic-menu' : undefined}
                      aria-haspopup='true'
                      aria-expanded={open ? 'true' : undefined}
                      onClick={handleClick}
                    >
                      {isHelperProfile
                        ? applyInfo?.helperInfo?.nickname ?? '탈퇴한 사용자'
                        : applyInfo?.requestInfo.writerInfo?.nickname ??
                          '탈퇴한 사용자'}
                    </Button>
                    {!userInfo?.isAdmin && (
                      <Menu
                        className='Offer_basic-menu'
                        anchorEl={anchorEl}
                        open={open}
                        onClose={handleClose}
                        MenuListProps={{
                          'aria-labelledby': 'basic-button',
                        }}
                      >
                        {helperIsMe ? (
                          <div>
                            <MenuItem onClick={() => setOpenOfferModal(true)}>
                              수정하기
                            </MenuItem>
                            <View_OfferForm
                              open={openOfferModal}
                              onClose={() => setOpenOfferModal(false)}
                              applyId={applyInfo?.id}
                              requestId={applyInfo?.requestInfo.requestId}
                              onConfirm={onConfirm}
                            />
                            <MenuItem onClick={onClickDelete}>
                              삭제하기
                            </MenuItem>
                          </div>
                        ) : (
                          <div>
                            <MenuItem onClick={onClickMessage}>
                              메시지 보내기
                            </MenuItem>
                            {requesterIsMe && (
                              <div>
                                <MenuItem onClick={onClickApprove}>
                                  수락하기
                                </MenuItem>
                                {openPaymentModal && (
                                  <PaymentModal
                                    onClose={() => setPaymentModal(false)}
                                    onConfirm={onApproveInternal}
                                    applyId={applyInfo?.id}
                                  />
                                )}
                                <MenuItem onClick={onClickReject}>
                                  거절하기
                                </MenuItem>
                              </div>
                            )}
                          </div>
                        )}
                      </Menu>
                    )}
                  </div>
                  <div className='Offer_address-box'>
                    <span className='Offer_address'>
                      {isHelperProfile
                        ? applyInfo?.helperInfo?.helperInfo.address
                        : applyInfo?.requestInfo.address1}
                    </span>
                    <LocationOnIcon className='Offer_location-icon' />
                  </div>
                </div>
                <div className='Offer_profile-right'>
                  <dl className='Offer_temperature-wrap'>
                    <dt className='Offer_trust-point'>신뢰도</dt>
                    <dd className='Offer_percent'>
                      {Math.round(
                        (isHelperProfile
                          ? applyInfo?.helperInfo?.trustPoint ?? 0
                          : applyInfo?.requestInfo.writerInfo?.trustPoint ??
                            0) * 10
                      ) / 10.0}
                    </dd>
                  </dl>
                </div>
              </div>
              <div className='Offer_condition'>
                <div className='Offer_condition-left'>
                  <Chip
                    className='Offer_payment'
                    label={'₩ ' + applyInfo?.amount.toLocaleString('ko-KR')}
                    color='primary'
                    variant='contained'
                  />
                </div>
              </div>
              <div className='Offer_article-comment-text'>
                <div className='Offer_article-detail'>
                  {applyInfo?.description}
                </div>
                <div className='Offer_count-box'>
                  <time className='Offer_reg-time'>
                    {applyInfo?.modTime
                      ? `${timeForToday(applyInfo?.modTime)}`
                      : timeForToday(applyInfo?.regTime)}
                  </time>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
