import React, { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Paper from '@mui/material/Paper';
import Stepper from '@mui/material/Stepper';
import Step from '@mui/material/Step';
import StepLabel from '@mui/material/StepLabel';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import AddressForm from './AddressForm';
import './CheckOut.css';
import HelperComplete from './HelperComplete';
import { registerHelper } from '../../axios/axios_actions';

const steps = ['헬퍼 등록', '완료'];

const theme = createTheme();

export default function CheckOut() {
  const navigate = useNavigate();

  const [activeStep, setActiveStep] = useState(0);
  const [address, setAddress] = useState();
  const [image, setImage] = useState();
  const [account, setAccount] = useState();

  const onConfirm = useCallback(() => {
    if (!address || !image || !account) {
      alert('내용을 모두 채워주세요.');
      return;
    }

    registerHelper(
      address,
      image.file,
      account.accountHolderName,
      account.fintechNum
    )
      .then(() => {
        setActiveStep(1);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [activeStep, address, image, account]);

  // const handleNext = () => {
  //   if (activeStep < 1) setActiveStep(activeStep + 1);
  // };

  // const handleBack = () => {
  //   if (activeStep > 0) setActiveStep(activeStep - 1);
  // };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Container component='main' maxWidth='sm' sx={{ mb: 20, mt: 20 }}>
        <Paper
          className='helper_form'
          variant='outlined'
          sx={{ my: { xs: 3, md: 6 }, p: { xs: 2, md: 3 } }}
        >
          <Typography component='h1' variant='h4' align='center'>
            헬퍼 신청
          </Typography>

          <Stepper activeStep={activeStep} sx={{ pt: 3, pb: 5 }}>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel>{label}</StepLabel>
              </Step>
            ))}
          </Stepper>
          <React.Fragment>
            {activeStep == steps.length ? (
              <React.Fragment>
                <Typography variant='h5' gutterBottom>
                  헬퍼 신청완료
                </Typography>
                <Typography variant='subtitle1'>
                  헬퍼 신청이 완료되었습니다. 관리자의 승인을 기다려주세요!
                </Typography>
              </React.Fragment>
            ) : (
              <React.Fragment>
                {activeStep == 0 ? (
                  <AddressForm
                    address={address}
                    setAddress={setAddress}
                    image={image}
                    setImage={setImage}
                    account={account}
                    setAccount={setAccount}
                  />
                ) : activeStep == 1 ? (
                  <HelperComplete />
                ) : null}

                <Box sx={{ displau: 'flex', justifyContent: 'flex-end' }}>
                  {activeStep ? (
                    <Button
                      id='checkout_btn'
                      variant='contained'
                      onClick={() => navigate('/myPage')}
                      sx={{ mt: 3 }}
                    >
                      마이페이지로 이동
                    </Button>
                  ) : (
                    <Button
                      id='checkout_btn'
                      variant='contained'
                      onClick={onConfirm}
                      sx={{ mt: 3 }}
                    >
                      신청하기
                    </Button>
                  )}
                </Box>
              </React.Fragment>
            )}
          </React.Fragment>
        </Paper>
      </Container>
    </ThemeProvider>
  );
}
