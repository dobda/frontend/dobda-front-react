import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { myInfo } from '../_redux/actions/user_actions';
import { resetAllChatInfo } from '../_redux/actions/modal_action';
import SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';

export default function (WrappedComponent) {
  function UserInfoCheck() {
    const dispatch = useDispatch();

    const isLoggedIn = useSelector((state) => state.user.isLoggedIn);
    const userInfo = useSelector((state) => state.user.userInfo);

    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
      if (!isLoggedIn) {
        dispatch(resetAllChatInfo());
        setLoaded(true);
        return;
      }

      dispatch(myInfo());
    }, [isLoggedIn]);

    useEffect(() => {
      if (!userInfo?.id) {
        return;
      }
      if (userInfo.isAdmin) {
        setLoaded(true);
        return;
      }
      const client = new StompJs.Client();
      client.webSocketFactory = function () {
        return new SockJS(process.env.REACT_APP_BACKEND_HOST + '/ws');
      };
      client.onConnect = () => {
        client.subscribe(`/ws/topic/${userInfo.id}/**`, () => {
          // console.log(msg.headers.destination); // 토픽 가져오기
          // console.log(msg.body); // 내용 가져오기
          dispatch(myInfo());
        });
      };
      client.activate();
      setLoaded(true);

      return () => {
        client.deactivate();
      };
    }, [userInfo?.id]);

    return loaded && WrappedComponent;
    // return WrappedComponent;
  }

  return <UserInfoCheck />;
}
