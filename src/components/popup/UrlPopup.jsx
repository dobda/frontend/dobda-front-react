import './UrlPopup.css';

import React, { useCallback } from 'react';
import { createPortal } from 'react-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

function UrlPopup({ url, onClose }) {
  const close = useCallback((event) => {
    event.preventDefault();
    onClose?.();
  });

  return createPortal(
    <div className='UrlPopup_root'>
      <div className='UrlPopup_bar'>
        <span>팝업</span>
        <div onClick={close}>
          <FontAwesomeIcon icon={faXmark} />
        </div>
      </div>
      <div>
        <iframe className='UrlPopup_iframe' src={url} />
      </div>
    </div>,
    document.getElementById('urlPopupModal')
  );
}

export default UrlPopup;
